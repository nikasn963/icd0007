<?php
/*$a = "5d238 ad9c0 \" '";
////////
print str_replace("\"","\"", $a);*/
function getBookById($searched_id) : bookadd
{
    $conn = getConnection();
    $stmt = $conn->prepare("SELECT book_id, title, author1, author2, book_list.grade, loetud, firstName, lastName FROM book_list left join author_list on author1 = author_id where book_id = :id;");
    $stmt->bindValue(":id", $searched_id);

    $stmt->execute();
    $array = [];
    foreach ($stmt as $row) {
        $id = $row["book_id"];
        $title = $row['title'];
        $author1 = $row["author1"];
        $author2 = $row["author2"];
        $author_name = $row["firstName"];
        $author_lastname = $row["lastName"];
        $grade = $row["grade"];
        $isRead = $row["loetud"];
        $values = new bookadd($id,$title,$author1,$author2,$grade,$isRead);
        $array[] = getBookAsLine($values);

    }
    //var_dump($array);

    foreach ($array as $line) {
        [$id, $title, $author1, $author2 ,$grade, $isRead] = explode(";", trim($line));
        if ($searched_id === $id) {
            $post = new bookadd($id,urldecode($title), urldecode($author1), urldecode($author2) ,urldecode($grade), urldecode($isRead));

            $post->id = $id;
            return $post;

        }


    }
    return "";
}




function getAllBooks() : array {

    $conn = getConnection();

    $stmt = $conn->prepare("SELECT book_id, title, author1, author2, book_list.grade, loetud, firstName, lastName FROM book_list left join author_list on author_list.author_id = book_list.author1;");

    $stmt->execute();
    $array = [];
    foreach ($stmt as $row) {
        $id = $row["book_id"];
        $title = $row['title'];
        if(!empty($row["author1"])){
            $author1 = $row["firstName"] . " " .  $row["lastName"];
        }else{
            $author1 = $row["author1"];
        }
        $author2 = $row["author2"];
        $grade = $row["grade"];
        $isRead = $row["loetud"];

        $values = new bookadd($id,$title,$author1,$author2,$grade,$isRead);
        $array[$id] = $values;

    }

    return array_values($array);
}

function getNewId() : string {
    $contents = file_get_contents(ID_FILE);
    $id = intval($contents);
    file_put_contents(ID_FILE, $id +1);
    return strval($id);
}

function saveBookPost(bookadd $post) : string {
    $characters = " \n\r\t\v\0";
    if($post->id){
        deleteBookById($post->id);
    }else{
        $post->id = getNewId();
    }

    deleteBookById($post->id);
    $conn = getConnection();
    var_dump($post->id);
    var_dump($post);
    $stmt = $conn->prepare('INSERT INTO book_list (book_id,title,author1, author2, grade, loetud) VALUES (:book_id,:title,:author1,:author2,:grade,:loetud);');
    var_dump($post->id);
    $stmt->bindValue(":book_id", intval($post->id) ?? "");
    $stmt->bindValue(":title", ltrim($post->title, $characters) ?? "");
    $stmt->bindValue(":author1", ltrim($post->author1, $characters) ?? "");
    $stmt->bindValue(":author2", ltrim($post->author2, $characters) ?? "");
    $stmt->bindValue(":grade", ltrim($post->grade, $characters) ?? "");
    $stmt->bindValue(":loetud", ltrim($post->isRead, $characters) ?? "");
    $stmt->execute();
    return $post->id;
}

function deleteBookById(string $id) : void {
    $conn = getConnection();

    $stmt = $conn->prepare('DELETE FROM book_list WHERE book_id= :id; ');
    $stmt->bindValue(":id", $id);
    $stmt->execute();

}

function getBookAsLine($post): string
{
    return urlencode($post->id) . ';' . urlencode($post->title) . ";" . urlencode($post->author1) . ";" . urlencode($post->author2) . ";" . urlencode($post->grade) . ";" . urlencode($post->isRead) . PHP_EOL;

}
function showBookAuthorOnMainPage(array $allBooks) :array{
    $conn = getConnection();
    $array2 = [];
    foreach ($allBooks as $line) {
        $stmt = $conn->prepare("SELECT author_id FROM author_list;");
        $stmt->execute();
        foreach ($stmt as $row) {
            if ($line->id === $row["author_id"]){
                $stmt = $conn->prepare("SELECT firstName, lastName FROM author_list where author_id = :id;");
                $stmt->bindValue(":id", $line->id ?? "");
                $stmt->execute();
                foreach ($stmt as $row){
                    $names = $row["firstName"] . $row["lastName"];
                    $array2[$line->id] = $names;
                }

            }
        }
    }
    return $array2;
}

//-------------------------------------------------------------------------------------

function getAuthorById($searched_id) : author_add_post
{
    $conn = getConnection();
    $stmt = $conn->prepare("SELECT * FROM author_list where author_id = :id;");
    $stmt->bindValue(":id", $searched_id);

    $stmt->execute();
    $array = [];
    foreach ($stmt as $row) {
        $id = $row["author_id"];
        $firstName = $row['firstName'];
        $lastName = $row["lastName"];
        $grade = $row["grade"];
        $values = new author_add_post($id,$firstName,$lastName,$grade);
        $array[] = getAuthorAsLine($values);

    }
    //var_dump($array);

    foreach ($array as $line) {
        [$id, $firstName, $lastName, $grade] = explode(";", trim($line));
        if ($searched_id === $id) {

            $post =new author_add_post($id,urldecode($firstName), urldecode($lastName), urldecode($grade));


            return $post;

        }


    }
    return "";
}

function saveAuthorPost(author_add_post $post) : string {

    if($post->id){
        deleteAuthorById($post->id);
    }else{
        $post->id = getNewId();
    }

    deleteAuthorById($post->id);


    file_put_contents(DATA_FILE, getAuthorAsLine($post), FILE_APPEND);

    return $post->id;
}

function deleteAuthorById(string $id) : void {
    $conn = getConnection();

    $stmt = $conn->prepare('DELETE FROM author_list WHERE author_id= :id; ');
    $stmt->bindValue(":id", $id);
    $stmt->execute();

}

function getAuthorAsLine($post): string
{
    return urlencode($post->id) . ';' . urlencode($post->firstName) . ";" . urlencode($post->lastName) . ";" . urlencode($post->grade) . PHP_EOL;

}

function getRowToAuthorList(author_add_post $post): string{
    if($post->id){
        deleteAuthorById($post->id);
    }else{
        $post->id = getNewId();
    }

    deleteAuthorById($post->id);
    $conn = getConnection();

    $stmt = $conn->prepare('INSERT INTO author_list (author_id,firstName, lastName, grade) VALUES (:author_id,:firstName,:lastName,:grade);');
    var_dump($post->id);
    $stmt->bindValue(":author_id", intval($post->id) ?? "");
    $stmt->bindValue(":firstName", $post->firstName ?? "");
    $stmt->bindValue(":lastName", $post->lastName ?? "");
    $stmt->bindValue(":grade", $post->grade ?? "");
    $stmt->execute();
    return $post->id;

}

function getAuthorsDB() :array
{
    $conn = getConnection();

    $stmt = $conn->prepare("SELECT * FROM author_list");

    $stmt->execute();
    $array = [];
    foreach ($stmt as $row) {
        $id = $row["author_id"];
        $firstName = $row['firstName'];
        $lastName = $row["lastName"];
        $grade = $row["grade"];
        $values = new author_add_post($id,$firstName,$lastName,$grade);
        $array[$id] = $values;

    }
    return array_values($array);

}



