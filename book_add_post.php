<?php


class bookadd {

    public string $id = '';
    public string $title;
    public string $grade;
    public string $isRead;
    public string $author1;
    public string $author2;
//////
    public function __construct(string $id, string $title,string $author1, string $author2 ,string $grade, string $isRead = "") {
        $this->id = $id;
        $this->title = $title;
        $this->author1 = $author1;
        $this->author2 = $author2;
        $this->grade = $grade;
        $this->isRead = $isRead;

    }

    public function __toString() : string {
        return sprintf('%s%s%s%s%s%s',$this->id ,$this->title,$this->author1 ,$this->author2 , $this->grade, $this->isRead,);
    }


}