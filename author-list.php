<?php
///
include_once __DIR__ . '/author_add_post.php';
include_once __DIR__ . '/author_list_print.php';
const DATA_FILE = __DIR__ . '/authors.txt';
const ID_FILE = __DIR__ . '/id-authors.txt';
include_once __DIR__ . "/allFunctions.php";
require_once __DIR__ . '/connection.php';

$message = $_GET["done"] ?? "";
$array = getAuthorsDB();



?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="style.css">

</head>
<body id="author-list-page">
<table class="tabel height headerTwoDiv"border="1">
    <tr>
        <td class="vtop">
            <table class="tabel main-colour" border="1">
                <tr>
                    <td colspan="3"><a href="index.php" id="book-list-link">Raamatud</a> &nbsp; | &nbsp; <a href="book-add.php" id="book-form-link">Lisa raamat</a> &nbsp; | &nbsp; <a href="author-list.php" id="author-list-link">Autorid</a> &nbsp; | &nbsp; <a href="author-add.php" id="author-form-link">Lisa autor</a></td>
                </tr>
            </table>
            <br>
            <table class="tabel">
                <tr>
                    <td class="W-40pr">Eesnimi</td>
                    <td class="W-40pr">Perekonnanimi</td>
                    <td class="W-20pr">Hinne</td>
                </tr>
            </table>
            <table class="tabel" border="1">
            <?php foreach ($array as $item){
                echo "<tr>";
                echo "<td class=\"W-40pr\"><a href='author-add.php?id=$item->id'>$item->firstName</a></td>".PHP_EOL;
                echo "<td class=\"W-40pr\">$item->lastName</td>".PHP_EOL;
                if($item->grade === ""){echo "<td class=\"W-20pr\"></td>"; echo "</tr>".PHP_EOL;}
                else if($item->grade === "1"){
                    echo "<td class=\"W-20pr\">";
                    echo "<span class=\"fa fa-star\" style=\"color: orange\"></span>".PHP_EOL;
                    echo "<span class=\"fa fa-star\"></span>".PHP_EOL;
                    echo "<span class=\"fa fa-star\"></span>".PHP_EOL;
                    echo "<span class=\"fa fa-star\"></span>".PHP_EOL;
                    echo "<span class=\"fa fa-star\"></span>".PHP_EOL;
                    echo "</td>".PHP_EOL;
                    echo "</tr>".PHP_EOL;
                }else if($item->grade === "2"){
                    echo "<td class=\"W-20pr\">";
                    echo "<span class=\"fa fa-star\" style=\"color: orange\"></span>".PHP_EOL;
                    echo "<span class=\"fa fa-star\" style=\"color: orange\"></span>".PHP_EOL;
                    echo "<span class=\"fa fa-star\"></span>".PHP_EOL;
                    echo "<span class=\"fa fa-star\"></span>".PHP_EOL;
                    echo "<span class=\"fa fa-star\"></span>".PHP_EOL;
                    echo "</td>".PHP_EOL;
                    echo "</tr>".PHP_EOL;
                }else if($item->grade === "3"){
                    echo "<td class=\"W-20pr\">";
                    echo "<span class=\"fa fa-star\" style=\"color: orange\"></span>".PHP_EOL;
                    echo "<span class=\"fa fa-star\" style=\"color: orange\"></span>".PHP_EOL;
                    echo "<span class=\"fa fa-star\" style=\"color: orange\"></span>".PHP_EOL;
                    echo "<span class=\"fa fa-star\"></span>".PHP_EOL;
                    echo "<span class=\"fa fa-star\"></span>".PHP_EOL;
                    echo "</td>".PHP_EOL;
                    echo "</tr>".PHP_EOL;
                } else if($item->grade === "4"){
                    echo "<td class=\"W-20pr\">";
                    echo "<span class=\"fa fa-star\" style=\"color: orange\"></span>".PHP_EOL;
                    echo "<span class=\"fa fa-star\" style=\"color: orange\"></span>".PHP_EOL;
                    echo "<span class=\"fa fa-star\" style=\"color: orange\"></span>".PHP_EOL;
                    echo "<span class=\"fa fa-star\" style=\"color: orange\"></span>".PHP_EOL;
                    echo "<span class=\"fa fa-star\"></span>".PHP_EOL;
                    echo "</td>".PHP_EOL;
                    echo "</tr>".PHP_EOL;
                }else if($item->grade === "5"){
                    echo "<td class=\"W-20pr\">";
                    echo "<span class=\"fa fa-star\" style=\"color: orange\"></span>".PHP_EOL;
                    echo "<span class=\"fa fa-star\" style=\"color: orange\"></span>".PHP_EOL;
                    echo "<span class=\"fa fa-star\" style=\"color: orange\"></span>".PHP_EOL;
                    echo "<span class=\"fa fa-star\" style=\"color: orange\"></span>".PHP_EOL;
                    echo "<span class=\"fa fa-star\" style=\"color: orange\"></span>".PHP_EOL;
                    echo "</td>".PHP_EOL;
                    echo "</tr>".PHP_EOL;
                }


            } ?>
            </table>

        </td>
    </tr>
    <tr>
        <td class="vbottom">
            <table class="tabel main-colour" border="1">
                <tr>
                    <td colspan="3" >ICD0007 Näidisrakendus</td>
                </tr>
                <?php
                if($message){
                    echo "<tr>";
                    echo "<td class=\"W-25pr\">Success:</td>";
                    echo "<td class=\"W-75pr\"><input class=\"tabel\" type=\"text\" id=\"message-block\" name=\"massage\" value=\" Author accepted\"></td>".PHP_EOL;
                    echo "</tr>";
                }
                ?>
            </table>
        </td>
    </tr>

</table>

</body>
</html>