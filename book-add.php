<?php
include_once __DIR__ . '/book_add_post.php';
const DATA_FILE = __DIR__ . '/books.txt';
const ID_FILE = __DIR__ . '/id-books.txt';
include_once __DIR__ . "/allFunctions.php";
require_once __DIR__ . '/connection.php';
include_once __DIR__ . '/author_add_post.php';
////
$data = $_GET['error'] ?? '';
$title = $_GET["title"] ?? "";
$isRead = $_GET["isRead"] ?? "";
$actualGrade = $_GET['grade'] ?? "";
$actualGrade = intval($actualGrade);
$id = $_GET["id"] ??"";
$array = getAuthorsDB();
/*foreach ($array as $item){
    var_dump($item->lastName);
    var_dump($item->firstName);
}*/

////
if (!empty($_POST)){ $post = new bookadd("",$_POST["title"],$_POST["author1"] ?? "",$_POST["author2"] ?? "" ,$_POST["grade"] ?? "", $_POST["isRead"] ?? "");}

if($id){
    $book = getBookById($id);



    $title = $book->title;
    $author1 = $book->author1;
    $author2 = $book->author2;
    $actualGrade = $book->grade;
    $actualGrade = intval($actualGrade);
    $isRead = $book->isRead;
}

if(isset($_POST["deleteButton"])){
    deleteBookById($id);
    header("Location: index.php?done=1");
}


if(isset($_POST["submitButton"]) && (strlen($_POST["title"]) >= 3 && strlen($_POST["title"]) < 24))
{
    if($id){
        $post = new bookadd("",str_replace("\" '","", $_POST["title"]),$_POST["author1"] ?? "",$_POST["author2"] ?? "" , $_POST["grade"] ?? "", $_POST["isRead"] ?? "");
        $post->id = $id;
        saveBookPost($post);
        header("Location: index.php?done=1");

    }else{
        $author1 = $_POST["author1"];
        $author2 = $_POST["author2"] ?? "";
        $grade = $_POST["grade"];
        $isRead = $_POST["isRead"];
        if(strpos($author1, "\"") !== false || strpos($author1, "'") !== false){
            $author1 = "";
        }
        if(strpos($author2, "\"") !== false || strpos($author2, "'") !== false){
            $author2 = "";
        }
        if(strpos($grade, "\"") !== false || strpos($grade, "'") !== false){
            $grade = "";
        }
        if(strpos($isRead, "\"") !== false || strpos($isRead, "'") !== false){
            $isRead = "";
        }

        $post = new bookadd("",$_POST["title"],$author1 ?? "",$author2 ?? "" , $grade ?? "", $isRead ?? "");
        //var_dump($post);
        saveBookPost($post);
        header("Location: index.php?done=1");
    }



}else if (isset($_POST["submitButton"]) && !(strlen($_POST["title"]) >= 3 && strlen($_POST["title"]) < 24)){
    $errors = urlencode("Tittle must be 3 till 23 symbols");
    header("Location: book-add.php?error=". $errors . "&title=" . $_POST["title"]. "&isRead=" . (!empty($_POST["isRead"]) ? "checked" : "")  . "&grade=" . $_POST["grade"]);
    exit;


}



?>




<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="style.css">

</head>
<body id="book-form-page">
<table class="tabel height headerTwoDiv" border="1">
    <tr>
        <td class="vtop">
            <table class="tabel main-colour" border="1">
                <tr>
                    <td colspan="3"><a href="../index.php" id="book-list-link">Raamatud</a> &nbsp; | &nbsp; <a href="../book-add.php" id="book-form-link">Lisa raamat</a> &nbsp; | &nbsp; <a href="../author-list.php" id="author-list-link">Autorid</a> &nbsp; | &nbsp; <a href="../author-add.php" id="author-form-link">Lisa autor</a></td>
                </tr>
            </table>
            <br>
            <br>
            <form method="post" action="book-add.php?id=<?= $id ?>">
                <table class="tabel" border="1">
                    <tr>
                        <td class="W-20pr"></td>
                        <td class="W-60pr">
                            <table class="tabel" border="1">
                                <tr>
                                    <td class="W-25pr">Pealkiri:</td>
                                    <td class="W-75pr"><input class="tabel" type="text" id="title" name="title" value="<?= $title ?>"></td>
                                    <?php
                                    if(!empty($data)){
                                        echo "<tr>";
                                        echo "<td class=\"W-25pr\">Error:</td>";
                                        echo "<td class=\"W-75pr\"><input class=\"tabel\" type=\"text\" id=\"error-block\" name=\"error\" value=\" $data\"></td>".PHP_EOL;
                                        echo "</tr>";
                                    }
                                    ?>
                                </tr>
                                <tr>
                                    <td>Autor1:</td>
                                    <td><select class="tabel" name = "author1">
                                            <?php
                                            $conn = getConnection();
                                            $firstName = "";
                                            $lastName = "";
                                            $stmt = $conn->prepare("SELECT firstName, lastName FROM book_list left join author_list on author_list.author_id = book_list.author1 where book_id = :id;");
                                            $stmt->bindValue(":id", $id);
                                            $stmt->execute();
                                            foreach ($stmt as $row){
                                                $firstName = $row["firstName"];
                                                $lastName = $row["lastName"];
                                            }
                                            $author1 = $author1 ?? "";
                                            if(!empty($author1)){
                                                echo "<option value=\"$author1\"> $firstName $lastName </option>";

                                                foreach ($array as $item){
                                                    if($author1 != $item->id){ echo "<option value=\"$item->id \"> $item->firstName $item->lastName </option>"; }

                                                }
                                            }else{
                                                echo "<option value=\" \">  </option>";
                                                foreach ($array as $item){
                                                    echo "<option value=\"$item->id \"> $item->firstName $item->lastName </option>";
                                                }
                                            }

                                            ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Autor2:</td>
                                    <td><select class="tabel">
                                            <option></option>
                                            <option value="o1">Option 1</option>
                                            <option value="o2">Option 2</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Hinne:</td>
                                    <td>
                                        <?php foreach (range(1, 5) as $grade): ?>

                                            <input type="radio"
                                                   name="grade"
                                                <?= $grade === $actualGrade ? 'checked' : ''; ?>
                                                   value="<?= $grade ?>" />
                                            <?= $grade ?>

                                        <?php endforeach; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Loetud:</td>
                                    <td><input type="checkbox" name="isRead" value="checked" <?= $isRead ?> ></td>
                                </tr>
                                <tr>
                                    <td colspan="2"><br></td>
                                </tr>
                                <tr>
                                    <td colspan="2"><br></td>
                                </tr>
                                <tr>
                                    <?php
                                    if($id){
                                        echo "<td class=\"pos_right\"><input type=\"submit\" value=\"Delete\" class=\"button\" name=\"deleteButton\"></td>";
                                    }
                                    echo "<td class=\"pos_right\"><input type=\"submit\" value=\"SalvestaB\" class=\"button\" name=\"submitButton\"></td>";
                                    ?>

                                </tr>
                            </table>
                        </td>
                        <td class="W-20pr"></td>
                    </tr>
                </table>
            </form>
    <tr>
        <td class="vbottom">
            <table class="tabel main-colour" border="1">
                <tr>
                    <td colspan="3" >ICD0007 Näidisrakendus</td>
                </tr>
            </table>
        </td>
    </tr>
    </td>
    </tr>

</table>

</body>
</html>
