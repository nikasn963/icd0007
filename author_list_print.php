<?php

class author_list_print
{
    //////
    public string $id ="";
    public string $firstName;
    public string $lastName;
    public string $grade;

    public function __construct(string $id,string $firstName, string $lastName, string $grade) {
        $this->id = $id;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->grade = $grade;

    }

    public function __toString() : string {
        return sprintf('%s%s%s%s',$this->id ,$this->firstName, $this->lastName, $this->grade);
    }

}