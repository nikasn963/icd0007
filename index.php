<?php
//include_once __DIR__ . "/allFunctions.php";
const DATA_FILE = __DIR__ . '/books.txt';
include_once __DIR__ . '/book_add_post.php';
require_once __DIR__ . '/connection.php';
require_once 'vendor/tpl.php';
require_once 'Request.php';
require_once 'DataBaseDao.php';
const ID_FILE = __DIR__ . '/id-books.txt';
include_once __DIR__ . '/author_add_post.php';
const ID_FILE_FOR_AUTHORS = __DIR__ . '/id-authors.txt';

$message = $_GET["done"] ?? "";
$request = new Request($_REQUEST);

$dao = new DataBaseDao();

$array_books = $dao->getAllBooks();
$array_authors = $dao->getAuthorsDB();
//$got_book = $dao->getBookById(104); // getBook to string is not working
$cmd = $request->param('cmd')
    ? $request->param('cmd')
    : 'show_book_list';

$id = $request->param('id')
    ? $request->param('id')
    : '';

if ($cmd === 'show_book_list') {
    $message = $_GET["done"] ?? "";
    $data = [
        'pageId' => 'book-list-page',
        'template' => 'book_list.html',
        'message' => $message,
        'data' => $array_books,
        'cmd' => 'show_book_by_id'
    ];

    print renderTemplate('tpl/main.html', $data);
}else if ($cmd === 'show_book_form') {
    $data = [
        'pageId' => 'book-form-page',
        'template' => 'book_form.html',
        'array' => $array_authors,
        'cmd' => 'book_add'
    ];

    print renderTemplate('tpl/main.html', $data);
}else if ($cmd === 'book_add') {
    if (!empty($_POST)){ $post = new bookadd("",$_POST["title"],$_POST["author1"] ?? "",$_POST["author2"] ?? "" ,$_POST["grade"] ?? "", $_POST["isRead"] ?? "");}
    if(isset($_POST["submitButton"]) && (strlen($_POST["title"]) >= 3 && strlen($_POST["title"]) < 24))
    {
        if(isset($id)){

            $post = new bookadd("",str_replace("\" '","", $_POST["title"]),$_POST["author1"] ?? "",$_POST["author2"] ?? "" , $_POST["grade"] ?? "", $_POST["isRead"] ?? "");
            $post->id = $id;
            $dao->saveBookPost($post);
            header("Location: index.php?cmd=show_book_list&done=1");

        }else{
            $author1 = $_POST["author1"];
            $author2 = $_POST["author2"] ?? "";
            $grade = $_POST["grade"];
            $isRead = $_POST["isRead"];
            if(strpos($author1, "\"") !== false || strpos($author1, "'") !== false){
                $author1 = "";
            }
            if(strpos($author2, "\"") !== false || strpos($author2, "'") !== false){
                $author2 = "";
            }
            if(strpos($grade, "\"") !== false || strpos($grade, "'") !== false){
                $grade = "";
            }
            if(strpos($isRead, "\"") !== false || strpos($isRead, "'") !== false){
                $isRead = "";
            }

            $post = new bookadd("",$_POST["title"],$author1 ?? "",$author2 ?? "" , $grade ?? "", $isRead ?? "");
            //var_dump($post);
            $dao->saveBookPost($post);
            header("Location: index.php?cmd=show_book_list&done=1");
        }



    }else if(isset($_POST["deleteButton"])){
        $dao->deleteBookById($id);
        header("Location: index.php?cmd=show_book_list&done=1");
    }else if (isset($_POST["submitButton"]) && !(strlen($_POST["title"]) >= 3 && strlen($_POST["title"]) < 24)){
        $errors = urlencode("Tittle must be 3 till 23 symbols");
        $data = [
            'pageId' => 'book-form-page',

            'errors' => urldecode($errors),
            'author1' => $_POST["author1"],
            'firstName' => $dao->authorFirstName($_POST["author1"]),
            'lastName' => $dao->authorLastName($_POST["author1"]),
            'array' => $array_authors,
            'cmd' => 'none',
            'title' => $_POST["title"],
            'isRead' => (!empty($_POST["isRead"]) ? "checked" : ""),
            'grade1' => $_POST["grade"]
        ];

        print renderTemplate('tpl/book_form.html', $data);

    }


} else if($cmd === "show_book_by_id"){

    $id = $_GET["id"] ??"";
    $got_book = $dao->getBookById($id);
    //var_dump($got_book);
    $data = [
        'pageId' => 'book-form-page',
        'array' => $array_authors,
        'got_book' => $got_book,
        'template' => 'book_edit.html',
        'title' => $got_book->title,
        'author1' => $got_book->author1,
        'author2' => $_POST["author2"] ?? "",
        'grade' => $got_book->grade, //$grade is not working auto
        'isRead' => $got_book->isRead,
        'cmd' => 'book_add',
        'id' =>  $id
    ];

    print renderTemplate('tpl/main.html', $data);


}else if($cmd == "show_author_list"){
    $message = $_GET["done"] ?? "";
    $data = [
        'pageId' => 'author-list-page',
        'template' => 'author_list.html',
        'message' => $message,
        'data' => $array_authors,
        'cmd' => 'author_edit'
    ];

    print renderTemplate('tpl/main.html', $data);
}else if($cmd == "show_author_form"){
    $message = $_GET["done"] ?? "";
    $data = [
        'pageId' => 'author-form-page',
        'template' => 'author_form.html',
        'cmd' => 'author_add'
    ];

    print renderTemplate('tpl/main.html', $data);


}else if($cmd == "author_add"){
    if (!empty($_POST)){ $post = new author_add_post("" ,$_POST["firstName"], $_POST["lastName"],  $_POST["grade"] ?? ""); }
    if(isset($_POST["deleteButton"])){

        $dao->deleteAuthorById($id);
        header("Location: index.php?cmd=show_author_list&done=3");
    }

    if(isset($_POST["submitButton"]) && (strlen($_POST["firstName"]) >= 1 && strlen($_POST["firstName"]) < 22) && (strlen($_POST["lastName"]) >= 2 && strlen($_POST["lastName"]) < 23))
    {
        if($id){
            $post = new author_add_post($id, $_POST["firstName"], $_POST["lastName"],  $_POST["grade"]);
            $post->id = $id;
            $dao->getRowToAuthorList($post);
            header("Location: index.php?cmd=show_author_list&done=2");

        }else{
            $grade = $_POST["grade"];
            if(strpos($grade, "\"") !== false || strpos($grade, "'") !== false){
                $grade = "";
            }

            $post = new author_add_post("", $_POST["firstName"] ?? "", $_POST["lastName"] ?? "",  $grade ?? "");
            $dao->getRowToAuthorList($post);
            header("Location: index.php?cmd=show_author_list&done=1");
        }
    }else if(isset($_POST["submitButton"]) && !((strlen($_POST["firstName"]) >= 1 && strlen($_POST["firstName"]) < 22) && (strlen($_POST["lastName"]) >= 2 && strlen($_POST["lastName"]) < 23)))
    {
        $errors = urlencode("Firstname must be 1 to 22 symbols and lastname 2 to 23 symbols");
        $data = [
            'errors' => urldecode($errors),
            'pageId' => 'author-form-page',
            'firstName' => $_POST["firstName"],
            'lastName' => $_POST["lastName"],
            'grade1' => $_POST["grade"]
        ];

        print renderTemplate('tpl/author_form.html', $data);


    }
} else if($cmd === "author_edit"){

    $id = $_GET["id"] ??"";
    $got_author = $dao->getAuthorById($id);
    //var_dump($got_book);
    $data = [
        'pageId' => 'author-form-page',
        'template' => 'author_edit.html',
        'firstName' => $got_author->firstName,
        'lastName' => $got_author->lastName,
        'grade1' => $got_author->grade, //$grade is not working auto
        'cmd' => 'author_add',
        'id' =>  $id
    ];

    print renderTemplate('tpl/main.html', $data);


}




