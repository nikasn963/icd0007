<?php
class DataBaseDao {

    public \PDO $connection;

    public function __construct() {
        $this->connection = getConnection();
    }


    function getAllBooks() : array {
        $stmt = $this->connection->prepare("SELECT book_id, title, author1, author2, book_list.grade, loetud, firstName, lastName FROM book_list left join author_list on author_list.author_id = book_list.author1;");

        $stmt->execute();
        $array = [];
        foreach ($stmt as $row) {
            $id = $row["book_id"];
            $title = $row['title'];
            if(!empty($row["author1"])){
                $author1 = $row["firstName"] . " " .  $row["lastName"];
            }else{
                $author1 = $row["author1"];
            }
            $author2 = $row["author2"];
            $grade = $row["grade"];
            $isRead = $row["loetud"];

            $values = new bookadd($id,$title,$author1,$author2,$grade,$isRead);
            $array[$id] = $values;

        }

        return array_values($array);
    }

    function authorFirstName($id) : string{
        $firstName = "";
        $stmt = $this->connection->prepare("SELECT firstName FROM author_list left join book_list on author_id = author1 where author1 = :id;");
        $stmt->bindValue(":id", $id);
        $stmt->execute();
        foreach ($stmt as $row){
            $firstName = $row["firstName"];

        }

        return $firstName;
    }
    function authorLastName($id) : string{
        $lastName = "";
        $stmt = $this->connection->prepare("SELECT lastName FROM book_list left join author_list on author_list.author_id = book_list.author1 where book_id = :id;");
        $stmt->bindValue(":id", $id);
        $stmt->execute();
        foreach ($stmt as $row){
            $lastName = $row["lastName"];
        }

        return $lastName;
    }

    function getAuthorsDB() :array
    {


        $stmt = $this->connection->prepare("SELECT * FROM author_list");
        $stmt->execute();
        $array = [];
        foreach ($stmt as $row) {
            $id = $row["author_id"];
            $firstName = $row['firstName'];
            $lastName = $row["lastName"];
            $grade = $row["grade"];
            $values = new author_add_post($id,$firstName,$lastName,$grade);
            $array[$id] = $values;

        }
        return array_values($array);

    }

    function saveBookPost(bookadd $post) : string {
        $characters = " \n\r\t\v\0";
        if($post->id){
            $this->deleteBookById($post->id);
        }else{
            $post->id = $this->getNewId();
        }

        $this->deleteBookById($post->id);
        //var_dump($post->id);
        //var_dump($post);
        $stmt = $this->connection->prepare('INSERT INTO book_list (book_id,title,author1, author2, grade, loetud) VALUES (:book_id,:title,:author1,:author2,:grade,:loetud);');
        //var_dump($post->id);
        $stmt->bindValue(":book_id", intval($post->id) ?? "");
        $stmt->bindValue(":title", ltrim($post->title, $characters) ?? "");
        $stmt->bindValue(":author1", ltrim($post->author1, $characters) ?? "");
        $stmt->bindValue(":author2", ltrim($post->author2, $characters) ?? "");
        $stmt->bindValue(":grade", ltrim($post->grade, $characters) ?? "");
        $stmt->bindValue(":loetud", ltrim($post->isRead, $characters) ?? "");
        $stmt->execute();
        return $post->id;
    }

    function getBookAsLine($post): string
    {
        return urlencode($post->id) . ';' . urlencode($post->title) . ";" . urlencode($post->author1) . ";" . urlencode($post->author2) . ";" . urlencode($post->grade) . ";" . urlencode($post->isRead) . PHP_EOL;

    }

    function getBookById($searched_id) : bookadd
    {
        $stmt = $this->connection->prepare("SELECT book_id, title, author1, author2, book_list.grade, loetud, firstName, lastName FROM book_list left join author_list on author1 = author_id where book_id = :id;");
        $stmt->bindValue(":id", $searched_id);

        $stmt->execute();
        $array = [];
        foreach ($stmt as $row) {
            $id = $row["book_id"];
            $title = $row['title'];
            $author1 = $row["author1"];
            $author2 = $row["author2"];
            $author_name = $row["firstName"];
            $author_lastname = $row["lastName"];
            $grade = $row["grade"];
            $isRead = $row["loetud"];
            $values = new bookadd($id,$title,$author1,$author2,$grade,$isRead);
            $array[] = $this->getBookAsLine($values);

        }
        //var_dump($array);

        foreach ($array as $line) {
            [$id, $title, $author1, $author2 ,$grade, $isRead] = explode(";", trim($line));
            if ($searched_id == $id) {
                $post = new bookadd($id,urldecode($title), urldecode($author1), urldecode($author2) ,urldecode($grade), urldecode($isRead));

                $post->id = $id;
                return $post;

            }


        }
        return "";
    }

    function getNewId() : string {
        $contents = file_get_contents(ID_FILE);
        $id = intval($contents);
        file_put_contents(ID_FILE, $id +1);
        return strval($id);
    }

    function getNewIdAuthors() : string {
        $contents = file_get_contents(ID_FILE_FOR_AUTHORS);
        $id = intval($contents);
        file_put_contents(ID_FILE_FOR_AUTHORS, $id +1);
        return strval($id);
    }

    function deleteBookById(string $id) : void {


        $stmt = $this->connection->prepare('DELETE FROM book_list WHERE book_id= :id;');
        $stmt->bindValue(":id", $id);
        $stmt->execute();

    }


    function getAuthorById($searched_id) : author_add_post
    {
        $stmt = $this->connection->prepare("SELECT * FROM author_list where author_id = :id;");
        $stmt->bindValue(":id", $searched_id);

        $stmt->execute();
        $array = [];
        foreach ($stmt as $row) {
            $id = $row["author_id"];
            $firstName = $row['firstName'];
            $lastName = $row["lastName"];
            $grade = $row["grade"];
            $values = new author_add_post($id,$firstName,$lastName,$grade);
            $array[] = $this->getAuthorAsLine($values);

        }
        //var_dump($array);

        foreach ($array as $line) {
            [$id, $firstName, $lastName, $grade] = explode(";", trim($line));
            if ($searched_id === $id) {

                $post = new author_add_post($id,urldecode($firstName), urldecode($lastName), urldecode($grade));


                return $post;

            }


        }
        return "";
    }



    function deleteAuthorById(string $id) : void {
        $stmt = $this->connection->prepare('DELETE FROM author_list WHERE author_id= :id; ');
        $stmt->bindValue(":id", $id);
        $stmt->execute();

    }

    function getAuthorAsLine($post): string
    {
        return urlencode($post->id) . ';' . urlencode($post->firstName) . ";" . urlencode($post->lastName) . ";" . urlencode($post->grade) . PHP_EOL;

    }

    function getRowToAuthorList(author_add_post $post): string{
        if($post->id){
            $this->deleteAuthorById($post->id);
        }else{
            $post->id = $this->getNewIdAuthors();
        }
        var_dump($post->id);
        $this->deleteAuthorById($post->id);
        $stmt = $this->connection->prepare('INSERT INTO author_list (author_id,firstName, lastName, grade) VALUES (:author_id,:firstName,:lastName,:grade);');
        $stmt->bindValue(":author_id", intval($post->id) ?? "");
        $stmt->bindValue(":firstName", $post->firstName ?? "");
        $stmt->bindValue(":lastName", $post->lastName ?? "");
        $stmt->bindValue(":grade", $post->grade ?? "");
        $stmt->execute();
        return $post->id;

    }


}