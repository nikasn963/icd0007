<?php
include_once __DIR__ . '/author_add_post.php';
const DATA_FILE = __DIR__ . '/authors.txt';
const ID_FILE = __DIR__ . '/id-authors.txt';
include_once __DIR__ . "/allFunctions.php";
require_once __DIR__ . '/connection.php';
include_once  __DIR__ . "/author_list_print.php";
///
$data = $_GET['error'] ?? '';
$firstName = $_GET["firstName"] ?? "";
$lastName = $_GET["lastName"] ?? "";
$actualGrade = $_GET['grade'] ?? "";
$actualGrade = intval($actualGrade);
$id = $_GET["id"] ??"";


if (!empty($_POST)){ $post = new author_add_post("" ,$_POST["firstName"], $_POST["lastName"],  $_POST["grade"] ?? ""); }

if($id){
    $author = getAuthorById($id);


    $firstName = $author->firstName;
    $lastName = $author->lastName;
    $actualGrade = $author->grade;
    $actualGrade = intval($actualGrade);

}

if(isset($_POST["deleteButton"])){
    deleteAuthorById($id);
    header("Location: author-list.php?done=1");
}

if(isset($_POST["submitButton"]) && (strlen($_POST["firstName"]) >= 1 && strlen($_POST["firstName"]) < 22) && (strlen($_POST["lastName"]) >= 2 && strlen($_POST["lastName"]) < 23))
{
    if($id){
        $post = new author_add_post($id, $_POST["firstName"], $_POST["lastName"],  $_POST["grade"]);

        //saveAuthorPost($post);
        getRowToAuthorList($post);
        header("Location: author-list.php?done=1");

    }else{
        $grade = $_POST["grade"];
        if(strpos($grade, "\"") !== false || strpos($grade, "'") !== false){
            $grade = "";
        }

        $post = new author_add_post("", $_POST["firstName"] ?? "", $_POST["lastName"] ?? "",  $grade ?? "");
        getRowToAuthorList($post);
        header("Location: author-list.php?done=1");
    }
}else if(isset($_POST["submitButton"]) && !((strlen($_POST["firstName"]) >= 1 && strlen($_POST["firstName"]) < 22) && (strlen($_POST["lastName"]) >= 2 && strlen($_POST["lastName"]) < 23)))
{
    $errors = urlencode("Firstname must be 1 to 22 symbols and lastname 2 to 23 symbols");
    header("Location: author-add.php?error=". $errors . "&firstName=" . $_POST["firstName"] . "&lastName=" . $_POST["lastName"] . "&grade=" . $_POST["grade"]);
    exit;


}




?>




<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="style.css">
</head>
<body id="author-form-page">
            <table class="tabel height headerTwoDiv">
                <tr>
                    <td class="vtop">
                        <table class="tabel main-colour" >
                            <tr>
                                <td colspan="3"><a href="../index.php" id="book-list-link">Raamatud</a> &nbsp; | &nbsp; <a href="../book-add.php" id="book-form-link">Lisa raamat</a> &nbsp; | &nbsp; <a href="../author-list.php" id="author-list-link">Autorid</a> &nbsp; | &nbsp; <a href="../author-add.php" id="author-form-link">Lisa autor</a></td>
                            </tr>
                        </table>
                        <br>
                        <br>
                        <form method="post" action="author-add.php?id=<?= $id ?>">
                        <table class="tabel">
                            <tr>
                                <td class="W-20pr"></td>
                                <td class="W-60pr">

                                    <table class="tabel">
                                        <tr>
                                            <td class="W-25pr">Eesnimi:</td>
                                            <td class="W-75pr"><input type="text" name="firstName" value="<?= $firstName ?>"></td>
                                        </tr>
                                        <tr>
                                            <td>Perekonnanimi:</td>
                                            <td><input type="text" name="lastName" value="<?= $lastName ?>"></td>
                                        </tr>
                                        <?php
                                        if(!empty($data)){
                                            echo "<tr>";
                                            echo "<td class=\"W-25pr\">Error:</td>";
                                            echo "<td class=\"W-75pr\"><input class=\"tabel\" type=\"text\" id=\"error-block\" name=\"error\" value=\" $data\"></td>".PHP_EOL;
                                            echo "</tr>";
                                        }
                                        ?>
                                        <tr>
                                            <td>Hinne:</td>
                                            <td>
                                                <?php foreach (range(1, 5) as $grade): ?>

                                                    <input type="radio"
                                                           name="grade"
                                                        <?= $grade === $actualGrade ? 'checked' : ''; ?>
                                                           value="<?= $grade ?>" />
                                                    <?= $grade ?>

                                                <?php endforeach; ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"><br></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"><br></td>
                                        </tr>
                                        <tr>
                                            <?php
                                            if($id){
                                                echo "<td colspan=\"2\" class=\"pos_right\"><input type=\"submit\" value=\"Delete\" class=\"button\" name=\"deleteButton\"></td>";
                                            }
                                            echo "<td colspan=\"2\" class=\"pos_right\"><input type=\"submit\" value=\"SalvestaA\" class=\"button\" name=\"submitButton\"></td>";
                                            ?>
                                        </tr>
                                    </table>
                                </td>
                                <td width="20%"></td>
                            </tr>
                        </table>
                        </form>
                        <tr>
                        <td class="vbottom">
                            <table class="tabel main-colour">
                            <tr>
                                <td colspan="3" >ICD0007 Näidisrakendus</td>
                            </tr>
                        </table>
                        </td>
                        </tr>

                    </td>
                </tr>
            </table>

</body>
</html>